#include "grafo.h"


void update_coord(vars *vars) {
    for (int x = 21; x < 81; x++) {  // je to bile
        for (int y = 296; y < 312; y++) {
            write_pixel(vars, x, y, 0xffff);
        }
    }
    char coords[10];
    sprintf(coords, "[%3d;%3d]", vars->penX, vars->penY);
    write_str(vars, 25, 296, coords, 0);
}

void color_chart_init(vars *vars) {
    int offsetX = 183;
    int offsetY = 296;
    for (int i = 0; i < 6; ++i) {
        for (int x = offsetX; x < offsetX + 16; ++x) {
            for (int y = offsetY; y < offsetY + 16; ++y) {
                write_pixel(vars, x, y, vars->COLORS_0X[i]);
            }
        }
        offsetX += 20;
    }
}

void update_color(vars *vars, int offset) {
    for (int x = 103; x < 162; x++) {
        for (int y = 296; y < 312; y++) {
            write_pixel(vars, x, y, vars->backgroundColor);
        }
    }
    vars->color = (vars->color + offset) % 6;
    write_str(vars, 103, 296, vars->COLORS_STR[vars->color], BLACK);
    *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_LED_RGB1_o) = vars->COLORS_0X_LED[vars->color];
    *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_LED_RGB2_o) = vars->COLORS_0X_LED[vars->color];
}

void write_pencil(vars *vars) {
    vars->penX = vars->penX < 1 ? 1 : vars->penX > WIDTH - 1 ? WIDTH - 1 : vars->penX;
    vars->penY = vars->penY < 1 ? 1 : vars->penY > HEIGHT_paper ? HEIGHT_paper : vars->penY;
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; j++) {
            write_pixel(vars, vars->penX + i, vars->penY + j, vars->COLORS_0X[vars->color]);
        }
    }
}

void write_pencil_pen_up(vars *vars) {
    vars->penX = vars->penX < 1 ? 1 : vars->penX > 477 ? 477 : vars->penX;
    vars->penY = vars->penY < 1 ? 1 : vars->penY > 477 ? 477 : vars->penY;
    for (int i = -2; i <= 2; ++i) {
        for (int j = -2; j <= 2; j++) {
            write_pixel(vars, vars->penX + i, vars->penY + j, vars->backgroundColor);
        }
    }
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; j++) {
            write_pixel(vars, vars->penX + i, vars->penY + j, vars->COLORS_0X[vars->color]);
        }
    }
}

void grafo(vars *vars, int option) {
    if (option == 0) { // new paper
        /* Setting the background color to white, setting the pen position to the middle of the screen, and setting the pen
        color to black. */
        color_and_draw_display(vars, vars->backgroundColor);
        vars->penX = WIDTH / 2;
        vars->penY = HEIGHT / 2;
        vars->color = 5; // black
        update_color(vars, 0);
        color_chart_init(vars);
        update_coord(vars);
    } else {
        free(vars->display);
        vars->display = vars->display_paper;
        vars->display_paper = NULL;
    }
    draw_display(vars);


    bool penUp = true;

    /* The above code is checking if the knob is turned or pressed, and if it is, it is updating the coordinates of the
    pencil. */
    while (true) {
        int knob_rotation = is_knob_turned_grafo(vars);

        int knob_pressed = is_knob_pressed(vars);


        /* Checking if the knob is turned, and if it is, it is updating the coordinates of the pencil. */
        if (knob_rotation != 0) {
            /* Checking which knob is turned, and it is updating the coordinates of the pencil. */
            switch (knob_rotation) {
                case 1:
                    vars->penX++;
                    break;
                case 2:
                    break;
                case 3:
                    vars->penY++;
                    break;
                case -1:
                    vars->penX--;
                    break;
                case -2:
                    break;
                case -3:
                    vars->penY--;
                    break;
                default:
                    exit(1);
            }
            if (penUp) {
                write_pencil_pen_up(vars);
            } else {
                write_pencil(vars);
            }
            update_coord(vars);
        }

        /* Checking if a knob is pressed, and if it is, it is checking which knob is pressed, and it is updating the
        color of the pencil, or it is lifting the pencil up. */
        if (knob_pressed != -1) {
            switch (knob_pressed) {
                case 1:
                    if (vars->red_knob_pressed) break;
                    vars->red_knob_pressed = true;
                    penUp = !penUp;
                    break;
                case 2:
                    if (vars->green_knob_pressed) break;
                    vars->green_knob_pressed = true;
                    update_color(vars, 1);
                    break;
                case 3:
                    if (vars->blue_knob_pressed) break;
                    vars->blue_knob_pressed = true;
                    vars->display_paper = (unsigned short *) malloc(HEIGHT * WIDTH * sizeof(unsigned short));
                    memcpy(vars->display_paper, vars->display, 2 * WIDTH * HEIGHT);
                    menu(vars);
                    break;
            }
        } else {
            vars->red_knob_pressed = false;
            vars->green_knob_pressed = false;
            vars->blue_knob_pressed = false;
        }
        if (knob_pressed != -1 || knob_rotation != 0) {
            draw_display(vars);
        }
    }
}
