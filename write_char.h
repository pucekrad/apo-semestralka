#ifndef APO_SEMESTRALKA_WRITE_CHAR_H
#define APO_SEMESTRALKA_WRITE_CHAR_H
#include "defines.h"
#include "display.h"
#include "string.h"

void write_char(vars *vars, int x, int y, char ch, uint16_t color);

void write_char_big(vars *vars, int x, int y, char ch, uint16_t color);

void write_char_bigger(vars *vars, int x, int y, char ch, uint16_t color);

void write_str(vars *vars, int x, int y, char *str, uint16_t color);

void write_str_big(vars *vars, int x, int y, char *str, uint16_t color);

void write_str_bigger(vars *vars, int x, int y, char *str, uint16_t color);

int char_width(vars *vars, int ch);

#endif //APO_SEMESTRALKA_WRITE_CHAR_H
