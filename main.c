#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "font_types.h"

#define WIDTH 480   // x
#define HEIGHT 320  // y

#define RED 0xF800
#define YELLOW 0xFFE0
#define GREEN 0x07E0
#define BLUE 0x07FF
#define PINK 0xF81F
#define BLACK 0x0000

unsigned short *display;
font_descriptor_t *fdes = &font_winFreeSystem14x16;
char *COLORS_STR[] = {"RED", "YELLOW", "GREEN", "BLUE", "PINK", "BLACK"};
int COLORS_0X[] = {RED, YELLOW, GREEN, BLUE, PINK, BLACK};
int penCOLOR, penX, penY;
int color = 0; // zvoleny index barva
int backgroundColor = 0xffff;
uint8_t red_knob;
uint8_t green_knob;
uint8_t blue_knob;

void write_pixel(int x, int y, unsigned short color) {
    if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT) {
        display[x + WIDTH * y] = color;
    }
}

/**
 * It writes a character to the screen
 *
 * @param x The x coordinate of the top left corner of the character
 * @param y The y coordinate of the top left corner of the character.
 * @param ch The character to write.
 * @param color The color of the character.
 */
void write_char(int x, int y, char ch, uint16_t color) {
    ch -= fdes->firstchar;
    unsigned w = (!fdes->width) ? fdes->maxwidth : fdes->width[ch];
    uint16_t *pd = fdes->bits + (ch * fdes->height);
    for (unsigned j = 0; j < fdes->height; ++j) {
        uint16_t d = *pd++;
        for (unsigned i = 0; i < w; ++i) {
            if (d & 0x8000) {
                write_pixel(x + i, y + j, color);  //  TODO skontrolovat vypis pismen
            } //else pozadi
            d <<= 1;
        }
    }
}

void knob_init(unsigned char *LED_mem_base) {
    int ptr = *(volatile uint32_t *)(LED_mem_base + SPILED_REG_KNOBS_8BIT_o);
    red_knob = ((ptr >> 16) & 0xff);
    green_knob = ((ptr >> 8) & 0xff);
    blue_knob = ((ptr >> 0) & 0xff);
}

// vykresli displej
void draw_display(unsigned char *parlcd_mem_base) {
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int ptr = 0; ptr < WIDTH * HEIGHT; ptr++) {
        parlcd_write_data(parlcd_mem_base, display[ptr]);
    }
}


/**
 * It writes the color on the display
 *
 * @param parlcd_mem_base the address of the memory mapped to the LCD
 * @param color the color to fill the screen with
 */
void color_and_draw_display(unsigned char *parlcd_mem_base, uint16_t color) {
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < WIDTH * HEIGHT; i++) {
        display[i] = color;
        parlcd_write_data(parlcd_mem_base, display[i]);
    }
}

void update_coord(int X, int Y) {
    for (int x = 21; x < 81; x++) {  // je to bile
        for (int y = 296; y < 312; y++) {
            write_pixel(x, y, 0xffff);
        }
    }
    write_char(21,296,'[', 0);
    char xx[10];
    sprintf(xx, "%3d;%3d]", X,Y);
    write_str(25, 296, xx, 0);
}

void write_str(int x, int y, char *str) { //TODO VSUDE PRIDAT BARVU
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        write_char(x, y, *str, 4);
        x += char_width(*str);
        str++;
    }
}


void color_chart_init() {
    int offsetX = 183;
    int offsetY = 296;
    for (int i = 0; i < 6; ++i) {
        for (int x = offsetX; x < offsetX+16; ++x) {
            for (int y = offsetY; y < offsetY+16; ++y) {
                write_pixel(x,y, COLORS_0X[i]);
            }
        }
        offsetX += 20;
    }
}

/**
 * It reads the value of the knob register, and returns the value of the knob that is currently pressed
 *
 * @return The knob number that is pressed.
 */
int is_knob_pressed(unsigned char *LED_mem_base) {
    int ptr = *(volatile uint32_t *)(LED_mem_base + SPILED_REG_KNOBS_8BIT_o);
    if ((ptr >> 26) & 0x1) {
        return 0;
    } else if ((ptr >> 25) & 0x1) {
        return 1;
    } else if ((ptr >> 24) & 0x1) {
        return 2;
    } else {
        return -1;
    }
}

void update_color(int n) {  //  n == 1 doprava || n == -1 dolava TODO
    for (int x = 103; x < 162; x++) {
        for (int y = 296; y < 312; y++) {
            write_pixel(x, y, backgroundColor);
        }
    }
    color = (color + n) % 6;
    write_str(103, 296, COLORS_STR[color]);
}

void write_pencil(int x, int y) { // x,y == +-1;
    penX = penX+x > 0 ? (penX+x < 479 ? penX+x : 478) : 1;
    penX = penX+x > 0 ? (penX+x < 287 ? penX+x : 286) : 1;
    for (int i=-1; i <=1; ++i) {
        for (int j = -1; j <= 1; j++) {
            write_pixel(penX+i, penY+j, COLORS_0X[color]);
        }
    }
}

int char_width(int ch) {
    int width = 0;
    if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size)) {
        ch -= fdes->firstchar;
        if (!fdes->width) {
            width = fdes->maxwidth;
        } else {
            width = fdes->width[ch];
        }
    }
    return width;
}

int main(int argc, char *argv[]) {
    unsigned char *parlcd_mem_base;
    unsigned char *LED_mem_base;
    display = (unsigned short *) malloc(HEIGHT * WIDTH * 2);

    /* It maps the physical address of the LCD to the virtual address. */
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    /* It maps the physical address of the LED to the virtual address. */
    LED_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    /* It initializes the LCD. */
    parlcd_hx8357_init(parlcd_mem_base);

    /* It initializes the knobs. */
    knob_init(LED_mem_base);

    /* It writes the background color on the display */
    color_and_draw_display(parlcd_mem_base, backgroundColor);

    update_coord(100,100);
    write_str(5, 5, ".-");
    color_chart_init();
    update_color(0);

    draw_display(parlcd_mem_base);

    while (1) {
        int knob = is_knob_pressed(LED_mem_base);
        if (knob != -1) {
            color_and_draw_display(parlcd_mem_base, 0xabcd);
            if (knob == 2) {
                exit(2);
            }
        }
    }


    printf("Goodbye world\n");

    return 0;
}