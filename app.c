#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "font_types.h"
#include "defines.h"
#include "menu.h"
#include "knobs.h"
#include "grafo.h"




int main(int argc, char *argv[]) {

    vars *vars = malloc(sizeof(*vars));
    vars->fdes = &font_winFreeSystem14x16;
    char *colors[] = {"RED", "YELLOW", "GREEN", "BLUE", "PINK", "BLACK"};
    vars->COLORS_STR = colors;
    int colorss[] = {RED_LED, YELLOW_LED, GREEN_LED, BLUE_LED, PINK_LED, BLACK_LED};
    vars->COLORS_0X_LED = colorss;
    int colorsss[] = {RED, YELLOW, GREEN, BLUE, PINK, BLACK};
    vars->COLORS_0X = colorsss;
    vars->penX = WIDTH/2;
    vars->penY = HEIGHT/2;
    vars->color = 5;
    vars->backgroundColor = 0xffff;
    vars->red_knob_pressed = false;
    vars->green_knob_pressed = false;
    vars->blue_knob_pressed = false;

    /* It allocates memory for the display. */
    vars->display = (unsigned short *) malloc(HEIGHT * WIDTH * 2);
    vars->display_paper = (unsigned short *) malloc(HEIGHT * WIDTH * 2);

    /* Mapping the physical address of the LCD to a virtual address. */
    vars->LCD_addr = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    /* Mapping the physical address of the knobs to a virtual address. */
    vars->KNOBS_addr = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    /* It initializes the LCD. */
    parlcd_hx8357_init(vars->LCD_addr);

    /* It initializes the knobs. */
    knob_init(vars);

    loading(vars);

    opening(vars);

    color_and_draw_display(vars, vars->backgroundColor);
    vars->penX = WIDTH / 2;
    vars->penY = HEIGHT / 2;
    vars->color = 5; // black
    update_color(vars, 0);
    color_chart_init(vars);
    update_coord(vars);

    memcpy(vars->display_paper, vars->display, WIDTH * HEIGHT * 2);

    menu(vars);

    return 0;
}