
#ifndef APO_SEMESTRALKA_MENU_H
#define APO_SEMESTRALKA_MENU_H
#include "display.h"
#include "write_char.h"
#include "defines.h"
#include "stdlib.h"
#include "stdio.h"
#include "unistd.h"
#include "knobs.h"
#include "grafo.h"
#include <stdio.h>

void opening(vars *vars);

void menu();

void save(vars *vars, int slot);

void load(vars *vars, int slot);

void loading(vars* vars);

void load_save_menu(vars *vars, bool ldsv);

void draw_arrow(vars *vars, int pos);

#endif //APO_SEMESTRALKA_MENU_H
