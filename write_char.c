#include "write_char.h"


void write_char(vars *vars, int x, int y, char ch, uint16_t color) {
    ch -= vars->fdes->firstchar;
    unsigned w = (!vars->fdes->width) ? vars->fdes->maxwidth : vars->fdes->width[ch];
    uint16_t *pd = vars->fdes->bits + (ch * vars->fdes->height);
    for (unsigned j = 0; j < vars->fdes->height; ++j) {
        uint16_t d = *pd++;
        for (unsigned i = 0; i < w; ++i) {
            if (d & 0x8000) {
                write_pixel(vars, x + i, y + j, color);
            }
            d <<= 1;
        }
    }
}

void write_char_big(vars *vars, int x, int y, char ch, uint16_t color) {
    ch -= vars->fdes->firstchar;
    unsigned w = (!vars->fdes->width) ? vars->fdes->maxwidth : vars->fdes->width[ch];
    uint16_t *pd = vars->fdes->bits + (ch * vars->fdes->height);
    for (unsigned j = 0; j < vars->fdes->height; ++j) {
        uint16_t d = *pd++;
        for (unsigned i = 0; i < w; ++i) {
            if (d & 0x8000) {
                write_pixel(vars, x + 2 * i, y + 2 * j, color);
                write_pixel(vars, x + 2 * i + 1, y + 2 * j, color);
                write_pixel(vars, x + 2 * i, y + 2 * j + 1, color);
                write_pixel(vars, x + 2 * i + 1, y + 2 * j + 1, color);
            }
            d <<= 1;
        }
    }
}

void write_char_bigger(vars *vars, int x, int y, char ch, uint16_t color) {
    ch -= vars->fdes->firstchar;
    unsigned w = (!vars->fdes->width) ? vars->fdes->maxwidth : vars->fdes->width[ch];
    uint16_t *pd = vars->fdes->bits + (ch * vars->fdes->height);
    for (unsigned j = 0; j < vars->fdes->height; ++j) {
        uint16_t d = *pd++;
        for (unsigned i = 0; i < w; ++i) {
            if (d & 0x8000) {
                for (int k = 0; k < 8; ++k) {
                    for (int l = 0; l < 8; ++l) {
                        write_pixel(vars, x + 8 * i + k, y + 8 * j + l, color);
                    }
                }
            }
            d <<= 1;
        }
    }
}

void write_str(vars *vars, int x, int y, char *str, uint16_t color) {
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        write_char(vars, x, y, *str, color);
        x += char_width(vars, *str);
        str++;
    }
}

void write_str_big(vars *vars, int x, int y, char *str, uint16_t color) {
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        write_char_big(vars, x, y, *str, color);
        x += char_width(vars, *str) * 2;
        str++;
    }
}

void write_str_bigger(vars *vars, int x, int y, char *str, uint16_t color) {
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        write_char_bigger(vars, x, y, *str, vars->COLORS_0X[i]);
        x += char_width(vars, *str) * 8;
        str++;
    }
}

int char_width(vars *vars, int ch) {
    int width = 0;
    if ((ch >= vars->fdes->firstchar) && (ch - vars->fdes->firstchar < vars->fdes->size)) {
        ch -= vars->fdes->firstchar;
        if (!vars->fdes->width) {
            width = vars->fdes->maxwidth;
        } else {
            width = vars->fdes->width[ch];
        }
    }
    return width;
}



