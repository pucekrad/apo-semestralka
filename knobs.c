#include "knobs.h"
#include <unistd.h>

void knob_init(vars *vars) {
    int ptr = *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_KNOBS_8BIT_o);
    vars->red_knob = ((ptr >> 16) & 0xff);
    vars->green_knob = ((ptr >> 8) & 0xff);
    vars->blue_knob = ((ptr >> 0) & 0xff);
}

void hulu(vars *vars, int t) {
    unsigned short *s = malloc(2 * WIDTH * HEIGHT);
    memcpy(s, vars->display, WIDTH * HEIGHT * 2);
    for (int i = 0; i < t; ++i) {
        color_and_draw_display(vars, vars->COLORS_0X[i % 6]);
        write_str_big(vars, random()%(480-4*16), random()%(320-32), "HULU", vars->COLORS_0X[(i+3)%6]);
        draw_display(vars);
        *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_LED_RGB1_o) = vars->COLORS_0X_LED[(i+3) % 6];
        *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_LED_RGB2_o) = vars->COLORS_0X_LED[(i+3) % 6];
        usleep(250000);
    }
    vars->display = s;
}

int is_knob_pressed(vars *vars) {
    int ptr = *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_KNOBS_8BIT_o);
    if (((ptr >> 26) & 0x1) && ((ptr >> 25) & 0x1) && ((ptr >> 24) & 0x1)) {
        hulu(vars, 20);
    } else if ((ptr >> 26) & 0x1) {
        return 1;
    } else if ((ptr >> 25) & 0x1) {
        return 2;
    } else if ((ptr >> 24) & 0x1) {
        return 3;
    } else {
        return -1;
    }
}

int knob_turned(vars *vars, int knob_id, uint8_t new_knob) {
    uint8_t knob;
    switch (knob_id) {
        case 1:
            knob = vars->red_knob;
            vars->red_knob = new_knob;
            break;
        case 2:
            knob = vars->green_knob;
            vars->green_knob = new_knob;
            break;
        case 3:
            knob = vars->blue_knob;
            vars->blue_knob = new_knob;
            break;
    }
    if ((new_knob - knob > 0 && new_knob - knob < 120) || new_knob - knob < -128) {
        return -1 * knob_id;
    } else {
        return 1 * knob_id;
    }
}

int is_knob_turned_grafo(vars *vars) {
    int ptr = *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_KNOBS_8BIT_o);
    uint8_t red = ((ptr >> 16) & 0xff);
    uint8_t green = ((ptr >> 8) & 0xff);
    uint8_t blue = ((ptr >> 0) & 0xff);
    return red != vars->red_knob ? knob_turned(vars, 1, red) : green != vars->green_knob ? knob_turned(vars, 2, green) :
                                                               blue != vars->blue_knob
                                                               ? knob_turned(vars, 3, blue)
                                                               : 0;
}

int is_knob_turned_menu(vars *vars) {
    int ptr = *(volatile uint32_t *) (vars->KNOBS_addr + SPILED_REG_KNOBS_8BIT_o);
    uint8_t red = ((ptr >> 16) & 0xff);
    uint8_t green = ((ptr >> 8) & 0xff);
    uint8_t blue = ((ptr >> 0) & 0xff);
    return (abs(red - vars->red_knob) > 10) ? knob_turned(vars, 1, red) : (abs(green - vars->green_knob) > 10)
                                                                          ? knob_turned(vars, 2, green)
                                                                          : (abs(blue - vars->blue_knob) >
                                                                             10) ? knob_turned(vars, 3,
                                                                                               blue)
                                                                                 : 0;
}


