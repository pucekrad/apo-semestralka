#ifndef APO_SEMESTRALKA_KNOBS_H
#define APO_SEMESTRALKA_KNOBS_H
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "defines.h"
#include "display.h"
#include "write_char.h"
#include "stdlib.h"

void knob_init(vars* vars);

int is_knob_pressed(vars *vars);

int knob_turned(vars *vars, int knob_id, uint8_t new_knob);

int is_knob_turned_grafo(vars *vars);

int is_knob_turned_menu(vars *vars);

void hulu(vars *vars, int t);
#endif //APO_SEMESTRALKA_KNOBS_H
