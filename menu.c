#include "menu.h"

void opening(vars *vars) {
    color_and_draw_display(vars, BLACK);
    write_str_bigger(vars, 50, 100, "GRAFO", 0xffff);
    draw_display(vars);
    sleep(5);
}

void menu(vars *vars) {
    color_and_draw_display(vars, BLACK);
    int top_offset = 46;
    int char_offset = 32 + 16;
    int arrow_idx = 0;
    write_str_big(vars, 200, top_offset, "NEW", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset, "LOAD", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset * 2, "SAVE", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset * 3, "CONTINUE", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset * 4, "EXIT", 0xffff);
    draw_arrow(vars, arrow_idx);
    draw_display(vars);

    while (true) {
        int knob_rotation = is_knob_turned_menu(vars);

        int knob_pressed = is_knob_pressed(vars);

        if (knob_rotation != 0) {
            switch (knob_rotation) {
                case 3:
                    ++arrow_idx;
                    draw_arrow(vars, arrow_idx % 5);
                    break;
                case -3:
                    arrow_idx += 4; // good practice (clever genius spectatular fenomenal awesome move)
                    draw_arrow(vars, arrow_idx % 5);
                    break;
            }
        }

        if (knob_pressed == 3 && !vars->blue_knob_pressed) {
            vars->blue_knob_pressed = true;
            switch (arrow_idx % 5) {
                case 0:
                    grafo(vars, 0);
                    break;
                case 1:
                    load_save_menu(vars, LOAD);
                    break;
                case 2:
                    load_save_menu(vars, SAVE);
                    break;
                case 3:
                    grafo(vars, 1);
                    break;
                case 4:
                    opening(vars);
                    hulu(vars, 1);
                    color_and_draw_display(vars, BLACK);
                    exit(0);
            }
        } else if (knob_pressed == -1) {
            vars->red_knob_pressed = false;
            vars->green_knob_pressed = false;
            vars->blue_knob_pressed = false;
        }

        if (knob_pressed != -1 || knob_rotation != 0) {
            draw_display(vars);
        }
    }
}

void save(vars *vars, int slot) {
    char filename[6];
    sprintf(filename, "slot%d", slot);
    FILE *f = fopen(filename, "wb");
    fwrite(vars->display_paper, WIDTH * HEIGHT, sizeof(unsigned short), f);
    fclose(f);
    menu(vars);
}

void load(vars *vars, int slot) {
    char filename[6];
    sprintf(filename, "slot%d", slot);
    FILE *f = fopen(filename, "rb");
    fread(vars->display_paper, WIDTH * HEIGHT, sizeof(unsigned short), f);
    fclose(f);
    grafo(vars, 1);
}

void loading(vars* vars) {
    for (int i = 100; i <= 380; ++i) {
        for (int x = 100; x < i; ++x) {
            for (int y = 150; y < 170; ++y) {
                write_pixel(vars, x, y, GREEN);
            }
        }
        for (int j = 210; j < 210 + 4 * 32; ++j) {
            for (int k = 80; k < 80 + 32; ++k) {
                write_pixel(vars, j, k, BLACK);
            }
        }
        char percent[5];
        sprintf(percent, "%.0f%%", (float) (i - 100) / 280 * 100);
        write_str_big(vars, 210, 80, percent, 0xffff);
        if (i < 200) {
            draw_display(vars);
        } else if (i < 290) {
            if (i % 2 == 0) {
                draw_display(vars);
            }
        } else if (i < 320) {
        } else {
            draw_display(vars);
            draw_display(vars);
        }
    }
    sleep(2);
}

void load_save_menu(vars *vars, bool ldsv) {
    color_and_draw_display(vars, BLACK);
    int top_offset = 46;
    int char_offset = 32 + 16;
    int arrow_idx = 0;
    write_str_big(vars, 200, top_offset, "SLOT 1", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset, "SLOT 2", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset * 2, "SLOT 3", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset * 3, "SLOT 4", 0xffff);
    write_str_big(vars, 200, top_offset + char_offset * 4, "SLOT 5", 0xffff);
    draw_arrow(vars, arrow_idx);
    draw_display(vars);

    while (true) {
        int knob_rotation = is_knob_turned_menu(vars);

        int knob_pressed = is_knob_pressed(vars);

        if (knob_rotation != 0) {
            switch (knob_rotation) {
                case 3:
                    ++arrow_idx;
                    draw_arrow(vars, arrow_idx % 5);
                    break;
                case -3:
                    arrow_idx += 4; // good practice (clever genius spectatular fenomenal awesome move)
                    draw_arrow(vars, arrow_idx % 5);
                    break;
            }
        }

        if (knob_pressed == 3 && !vars->blue_knob_pressed) {
            vars->blue_knob_pressed = true;
            if (ldsv) {
                save(vars, arrow_idx % 5);
            } else {
                load(vars, arrow_idx % 5);
            }
        } else if (knob_pressed == -1) {
            vars->red_knob_pressed = false;
            vars->green_knob_pressed = false;
            vars->blue_knob_pressed = false;
        }

        if (knob_pressed != -1 || knob_rotation != 0) {
            draw_display(vars);
        }
    }
}

void draw_arrow(vars *vars, int pos) {

    for (int i = 165; i < 165 + char_width(vars, '>') * 2; ++i) {
        for (int j = 46; j < HEIGHT - 46; ++j) {
            write_pixel(vars, i, j, BLACK);
        }
    }

    int top_offset = 46;
    int char_offset = 32 + 16;
    write_char_big(vars, 165, top_offset + char_offset * pos, '>', 0xffff);
}


