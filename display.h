

#ifndef APO_SEMESTRALKA_DISPLAY_H
#define APO_SEMESTRALKA_DISPLAY_H
#include "defines.h"

/**
 * It takes in an x and y coordinate and a color, and if the x and y coordinates are within the bounds of the screen, it
 * writes the color to the display pointer
 *
 * @param x The x coordinate of the pixel to be written.
 * @param y The y coordinate of the pixel.
 * @param color The color of the pixel.
 */
void write_pixel(vars *vars, int x, int y, unsigned short color);

/**
 * It writes the contents of the display array to the LCD
 */
void draw_display(vars *vars);

/**
 * It writes the color to the display
 *
 * @param vars a pointer to the vars struct
 * @param color the color to fill the screen with
 */
void color_and_draw_display(vars *vars, uint16_t color);


#endif //APO_SEMESTRALKA_DISPLAY_H


