#ifndef APO_SEMESTRALKA_DEFINES_H
#define APO_SEMESTRALKA_DEFINES_H

#define _POSIX_C_SOURCE 200112L

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "stdbool.h"

#define WIDTH 480   // x
#define HEIGHT 320  // y
#define HEIGHT_paper 288 // y_paper

#define RED 0xF800
#define YELLOW 0xFFE0
#define GREEN 0x07E0
#define BLUE 0x07FF
#define PINK 0xF81F
#define BLACK 0x0000

#define RED_LED 0xFF0000
#define YELLOW_LED 0xFFFF00
#define GREEN_LED 0x00ff00
#define BLUE_LED 0x0000FF
#define PINK_LED 0xFF00FF
#define BLACK_LED 0x000000

#define SAVE true
#define LOAD false


//A struct that contains all the variables that are used in the program.
typedef struct vars{
    /* A pointer to the display memory. */
    unsigned short *display;
    unsigned short *display_paper;

/* A pointer to a font descriptor. */
    font_descriptor_t *fdes;
/* A pointer to the LCD memory. */
    unsigned char *LCD_addr;
/* A pointer to the knobs. */
    unsigned char *KNOBS_addr;

    char **COLORS_STR;

    int *COLORS_0X_LED;
    int *COLORS_0X;
/* Setting the pen position to the top left corner of the screen. */
    int penX;

    int penY;
/* Setting the default color to black. */
    int color;
/* Setting the background color to white. */
    int backgroundColor;
    uint8_t red_knob;
    uint8_t green_knob;
    uint8_t blue_knob;

    bool red_knob_pressed;
    bool green_knob_pressed;
    bool blue_knob_pressed;

}vars;

#endif //APO_SEMESTRALKA_DEFINES_H
