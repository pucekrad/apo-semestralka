
#ifndef APO_SEMESTRALKA_GRAFO_H
#define APO_SEMESTRALKA_GRAFO_H
#include "defines.h"
#include "write_char.h"
#include "display.h"
#include "stdio.h"
#include "stdlib.h"
#include "knobs.h"
#include "string.h"
#include "menu.h"

/**
 * It draws a white rectangle over the previous coordinates and then draws the new coordinates
 *
 * @param vars pointer to the vars structure
 */
void update_coord(vars *vars);

/**
 * It draws a color chart
 *
 * @param vars The vars struct that contains all the variables needed for the program to run.
 */
void color_chart_init(vars *vars);

/**
 * It updates the color of the brush and the LEDs
 *
 * @param vars a pointer to the vars struct
 * @param offset the offset to add to the current color.
 */
void update_color(vars *vars, int offset);

/**
 * It writes a 3x3 pixel square to the screen, with the center pixel being the current position of the pencil
 *
 * @param vars a pointer to the vars struct
 */
void write_pencil(vars *vars);

/**
 * It draws a 5x5 pixel square with the center pixel being the color of the pen and the rest being the background color
 *
 * @param vars a pointer to the vars struct
 */
void write_pencil_pen_up(vars *vars);

/**
 * It's a function that draws a picture on the screen, and it's controlled by the knobs
 *
 * @param vars a pointer to the struct that contains all the variables that are used in the program.
 * @param option 0 if the user is starting a new paper, 1 if the user is returning to the paper.
 */
void grafo(vars *vars, int option);

#endif //APO_SEMESTRALKA_GRAFO_H
