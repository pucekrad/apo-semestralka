#include "display.h"


void write_pixel(vars *vars, int x, int y, unsigned short color) {
    if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT) {
        vars->display[x + WIDTH * y] = color;
    }
}

void draw_display(vars *vars) {
    parlcd_write_cmd(vars->LCD_addr, 0x2c);
    for (int ptr = 0; ptr < WIDTH * HEIGHT; ptr++) {
        parlcd_write_data(vars->LCD_addr, vars->display[ptr]);
    }
}

void color_and_draw_display(vars *vars, uint16_t color) {
    parlcd_write_cmd(vars->LCD_addr, 0x2c);
    for (int i = 0; i < WIDTH * HEIGHT; i++) {
        vars->display[i] = color;
        parlcd_write_data(vars->LCD_addr, vars->display[i]);
    }
}